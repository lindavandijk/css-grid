# SCSS Grid Layout with fallback for IE

This repository includes a responsive css grid layout with a fallback for browsers without support for grid-gap like IE. 

## Setup

##### Install

Install with npm:

`$ npm install --save scss-grid-layout` 

Install with yarn:

`$ yarn add scss-grid-layout`

##### Import

`@import "../../../node_modules/scss-grid-layout/src/layout";`

> Import this file after your own variables which overwrite the default


##### Overwrite the !default variables

```scss
$grid-max-container-width: 960px;

$grid-columns: 12;

$grid-breakpoints: (
  xs: 0,
  sm: 768px,
  md: 1024px,
  lg: 1440px
);

$grid-gap: (
  xs: 16px,
  sm: 24px,
  md: 24px,
  lg: 40px
);

$grid-offset: (
  xs: 16px,
  sm: 30px,
  md: 40px,
  lg: 0
);
```

## Usage

##### Grid

```html
<div class="row">
	<div class="col--12 col-sm--6"></div>
	<div class="col--12 col-sm--6"></div>
</div>
```

or 

```html
<div class="row">
	<div class="col--2-5 col-sm--2-8"></div>
</div>
```

`col--2-5` Gives you a column which start at column 2 and ends at column 5.


##### Breakpoints

```
@include grid-media(md) {           // @media and (min-width: 1024px)
    ...
}
```


or

```
@include grid-media(md, max) {     // @media and (max-width: 1024px)
    ...
}
```
If you add `max` as second variable, it gives you `max-width`

## Support


Chrome | Safari | Firefox | Edge | IE
| :---: | :---: | :---: | :---: | :---:
Yes | Yes | Yes | Yes | ✨ **YES** ✨

> There is a fallback for browsers without support for grid-gap. For this browsers the layout is styled with Flexbox instead of Css grid.









